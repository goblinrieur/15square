# 15 square game

I don't think I've to explain that ? 

Just sort & align 15 incremented values in a 4 x 4 square containing an empty cell.

![picture of game](./15.png) ![picture of game](./15-2.png)

# Forth

use [gforth](https://www.gnu.org/software/gforth/) 

current version used gforth 0.7.3, also validated on 0.7.9 

original code has unknown real origin, it is well known & available from multiple sources

here is my own fork of it.

# License 

see [LICENSE](LICENSE) file 

# WIP : 2023-09-02

make it public & permit it to be injected in [gforth gadget container](https://gitlab.com/goblinrieur/gforth_gadgets)

and of course being ran by his own.

# DEV notes

- [X] Having a running version
- [X] Indent existing part of code
- [X] Add a move count might be interesting 
- [X] Added colors
- [X] Update its display for a better view 
- [X] Update to be playable with arrow keys too
- [X] Updated alignment & Unicode emot-icons numbers


```
       _   ___   _ 
__   _/ | / _ \ / |
\ \ / / || | | || |
 \ V /| || |_| || |
  \_/ |_(_)___(_)_|
                   
```
                      
updated : Wed Aug 22 11:05:57 PM CEST 2023
